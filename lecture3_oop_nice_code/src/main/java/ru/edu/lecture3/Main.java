package ru.edu.lecture3;

import java.time.LocalDate;
import java.util.ArrayList;

import static ru.edu.lecture3.Gender.*;

public class Main {
    public static void main(String[] args) {

        User vasek = new User("VasyaBiceps","Vasya", "Ppetrov", LocalDate.of(1990,9,12), MALE);
        User leNa = new User("lenatop","Leno4ka", "Ivanova", LocalDate.of(1991,5,30), FEMALE);
        User sveta = new User("sveto4","Svetlana", "Petuhova", LocalDate.of(2000,3,1), FEMALE);

        UserStorage storage = new UserStoragempl();

        storage.put(vasek);
        storage.put(leNa);
        storage.put(sveta);

        System.out.println(storage.getUserByLogin("lenatop"));
        System.out.println(storage.getAllUsersByGender(FEMALE));
        System.out.println("Возраст " + storage.getUserAge("sveto4"));
        System.out.println(storage.getAllUsers());
        System.out.println("проводим удаление");
        storage.remove("VasyaBiceps");
        System.out.println(storage.getAllUsers());


    }
}
