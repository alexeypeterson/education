package ru.edu.lecture3;


import java.time.*;
import java.util.*;
import java.util.stream.Collectors;


public class UserStoragempl implements UserStorage{

    private final ArrayList<User> userStorage = new ArrayList<>();

         /**
         * Get user by login. Notice that anton & anToN are same logins.
         *
         * @throws RuntimeException         if user not found
         * @throws IllegalArgumentException if login is null, empty or blank.
         */
    @Override
    public User getUserByLogin(String login) {

        if (login == null | login.equals("")){
            throw new IllegalArgumentException("Не корректный логин(не может быть пустым).");
        }
            User userFromStorage = userStorage.stream().filter(it -> it.getLogin().equals(login)).findFirst().orElse(null);
        if (userFromStorage == null) {
            throw new RuntimeException("Пользователь не найден: " + login);
        }
        return userFromStorage;
    }


    /**
     * Put new user into storage.
     *
     * @param user - user for store
     * @throws RuntimeException if user has wrong data, e.g. null or empty login, names, birthdate or gender.
     */
    @Override
    public User put(User user) {
        if (user.getLogin() == null | Objects.equals(user.getLogin(), "")){
            throw new IllegalArgumentException("Не корректный логин(не может быть пустым).");
        }
        userStorage.add(user);
        return user;
    }

    /**
     * Remove user by login.
     *
     * @param login - login
     * @throws RuntimeException         if user not found
     * @throws IllegalArgumentException if login is null, empty or blank.
     */
    @Override
    public User remove(String login) {
        User user = getUserByLogin(login);
        if (user.getLogin() == null | Objects.equals(user.getLogin(), "")){
            throw new IllegalArgumentException("Не корректный логин(не может быть пустым).");
        }
        userStorage.remove(user);
        return user;
    }

    /**
     * Get all users.
     */
    @Override
    public List<User> getAllUsers() {
        return userStorage;
    }

    /**
     * Get all users by gender.
     *
     * @param gender - gender
     * @throws IllegalArgumentException if gender is null
     */
    @Override
    public List<User> getAllUsersByGender(Gender gender) {
        if (gender == null) {
            throw new IllegalArgumentException("Пол не может быть пустым");
        }
        return userStorage.stream().filter(it -> it.getGender().equals(gender)).collect(Collectors.toList());
    }

    /**
     * Get user age.
     *
     * @param login - login
     * @throws RuntimeException         if user not found
     * @throws IllegalArgumentException if login is null, empty or blank.
     */
    @Override
    public int getUserAge(String login) {
        User user = getUserByLogin(login);
        if (user.getLogin() == null | Objects.equals(user.getLogin(), "")){
            throw new IllegalArgumentException("Не корректный логин(не может быть пустым).");
        }
        return Period.between(user.getBirthDate(), LocalDate.now()).getYears();
    }
}
