import org.junit.*;

import org.junit.jupiter.api.Assertions;
import org.junit.rules.ExpectedException;
import ru.edu.lecture3.Gender;
import ru.edu.lecture3.User;
import ru.edu.lecture3.UserStorage;

import ru.edu.lecture3.UserStoragempl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;



public class UserStoragemplTests {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    private UserStorage userStorage;
    private final String LOGIN = "kate";
    private final User USER = new User(LOGIN, "Ekaterina", "Velikaya", LocalDate.of(1621, 4, 1), Gender.FEMALE);

    @Before
    public void beforeMethod() {
        userStorage = new UserStoragempl();
    }


     /**
     * Успешное выполнение метода put
     */

    @Test
    public void put_Test() {
        userStorage.put(USER);
        Assert.assertEquals(userStorage.getUserByLogin(LOGIN), USER);
    }

     /**
     * Неуспешное выполнение метода put
     */

    @Test(expected = RuntimeException.class)
    public void put_Negative_Test() {
            User userNew = new User("", "Petr", "Shibakov", LocalDate.of(1987, 9, 14), Gender.MALE);
            userStorage.put(userNew);
            exceptionRule.expect(RuntimeException.class);
            exceptionRule.expectMessage("Некорректный логин(не может быть пустым)");
    }

     /**
     * Успешное выполнение метода getUserByLogin
     */

    @Test
    public void getUserByLogin_Test() {
        userStorage.put(USER);
        Assert.assertEquals(userStorage.getUserByLogin(LOGIN), USER);
    }

     /**
     * Неуспешное выполнение метода getUserByLogin
     */

    @Test(expected = RuntimeException.class)
    public void getUserByLoginNegative_Test() {
        String login = "wrongLogin";
        userStorage.put(USER);
        Assert.assertEquals(userStorage.getUserByLogin(login), USER);
        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Пользователь " + login + " не найден");

    }


     /**
     * Успешное выполнение метода getUserAge
     */

    @Test
    public void getUserAge_Test() {
        userStorage.put(USER);
        LocalDate birthDate = USER.getBirthDate();
        int age = LocalDate.now().getYear() - birthDate.getYear();
        Assert.assertEquals(age, userStorage.getUserAge(LOGIN));
    }

     /**
     * Неуспешное выполнение метода getUserAge
     */

    @Test(expected = RuntimeException.class)
    public void getUserAgeNegative_Test() {
        userStorage.getUserAge("wrongLogin");
        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Пользователь не найден");
    }


     /**
     * Успешное выполнение метода remove
     */

    @Test
    public void remove_Test() {
        userStorage.put(USER);
        Assertions.assertEquals(USER, userStorage.remove("kate"));

    }

     /**
     * Неуспешное выполнение метода remove
     */

    @Test(expected = RuntimeException.class)
    public void removeNegative_Test() {
        userStorage.remove("");
        exceptionRule.expectMessage("Некорректный логин(не может быть пустым)");
    }


     /**
     * Успешное выполнение метода getAllUsers
     */

    @Test
    public void getAllUsers_Test() {
        List<User> testStorage = new ArrayList<>();
        testStorage.add(USER);
        userStorage.put(USER);
        Assertions.assertEquals(testStorage, userStorage.getAllUsers());
    }

     /**
     * Неуспешное выполнение метода getAllUsers
     */

    @Test
    public void getAllUsersNegative_Test() {
        List<User> testStorage = new ArrayList<>();
        testStorage.add(USER);
        Assertions.assertNotEquals(testStorage, userStorage.getAllUsers());
    }


     /**
     * Успешное выполнение метода getAllUsersByGender
     */

    @Test
    public void getAllUsersByGender_Test() {
        userStorage.put(USER);
        List<User> sizeUserList = userStorage.getAllUsersByGender(Gender.FEMALE);
        Assert.assertFalse(sizeUserList.isEmpty());
    }

     /**
     * Неуспешное выполнение метода getAllUsersByGender
     */

    @Test(expected = AssertionError.class)
    public void getAllUsersByGenderNegative_Test() {
        List<User> sizeUserList = userStorage.getAllUsersByGender(Gender.MALE);
        Assert.assertFalse(sizeUserList.isEmpty());
        exceptionRule.expectMessage("Пользователь не найден");
    }

}