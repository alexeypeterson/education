package ru.edu.lecture3;

import java.io.*;
import java.util.*;

public class FileAnalysermpl implements FileAnalyser{

    private final File File;

    public FileAnalysermpl(String fileName) {
        File = new File(fileName);
    }

    @Override
    public String getFileName() {
        if(File.exists()){
            return File.getName();
        }
        throw new RuntimeException("Файл не существует");

    }

    @Override
    public int getRowsCount() {
        int countORows = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(getFileName()))) {
            String line = reader.readLine();
            while (line != null) {
                line = reader.readLine();
                countORows++;
                }
        } catch (Exception ex) {
            throw new IllegalArgumentException("Файл не существует.");
        }
        return countORows;
    }

    @Override
    public int getLettersCount() {
        int countOfLetters = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(getFileName()))) {
            int symbol = reader.read();
            while (symbol != -1) {
                countOfLetters++;
                symbol = reader.read();
            }
        } catch (IOException ex) {
            throw new IllegalArgumentException("Файл не существует.");
        } catch (Exception ex) {
            throw new RuntimeException("Ошибка при выполнении метода getLettersCount");
        }
        return countOfLetters;
    }


    @Override
    public Map<Character, Integer> getSymbolsStatistics() {
        Map<Character, Integer> symbStat = new HashMap<>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(getFileName()));
            int symbol = reader.read();
            while (symbol != -1) {
                int count = symbStat.get((char) symbol) != null ? symbStat.get((char) symbol) : 0;
                symbStat.put((char) symbol, count + 1);
                symbol = reader.read();
                //reader.close();
            }
        } catch (IOException ex) {
            throw new IllegalArgumentException("Файл не существует.");
        }finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return symbStat;
    }

    @Override
    public List<Character> getTopNPopularSymbols(int n) {
        Map<Character, Integer> symbStat = getSymbolsStatistics();
        List<Character> popularSymbols = new ArrayList<>();
        List<Integer> countSet = new ArrayList<>(symbStat.values());
        countSet.sort(Integer::compareTo);
        Collections.reverse(countSet);
        for (int i = 0; i < n; i++) {
            for (Map.Entry<Character, Integer> entry : symbStat.entrySet()) {
                if (countSet.get(i).equals(entry.getValue())){
                    popularSymbols.add(entry.getKey());
                    symbStat.remove(entry.getKey());
                    break;
                }
            }
        }
        return popularSymbols;

    }

    @Override
    public void saveSummary(String filePath) {
        try {
            FileWriter writer = new FileWriter(filePath);
            writer.write("fileName: "+getFileName() + "\n");
            writer.write("rowsCount: "+getRowsCount() + "\n");
            writer.write("totalSymbols: "+getLettersCount() + "\n");
            writer.write("symbolsStatistic: "+getSymbolsStatistics().toString().replace("=",":") + "\n");
            writer.write("top3PopularSymbols: "+getTopNPopularSymbols(3).toString()
                    .replace("[","")
                    .replace("]",""));
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
