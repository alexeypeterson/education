package ru.edu.lecture3;
import java.io.*;


public class Main {
    public static void main(String[] args) {

        String fileName = "Storage.txt";
        try {
            FileWriter writer = new FileWriter(fileName);
            writer.write("Not are test");
            writer.append('\n');
            writer.write("symbol input");
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();

        }
        String outFile = "result.txt";
        int topNsymbols = 4;
        FileAnalysermpl fa = new FileAnalysermpl(fileName);
        System.out.println("Имя файла: " + fa.getFileName());
        System.out.println("Количество строк: " + fa.getRowsCount());
        System.out.println("Количество букв: " + fa.getLettersCount());
        System.out.println("Статистика символов: " + fa.getSymbolsStatistics());
        System.out.println("Топ " + topNsymbols + " популярных символов: " + fa.getTopNPopularSymbols(topNsymbols));
        fa.saveSummary(outFile); // создание результирующего файла.
    }
}
