import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.edu.lecture3.FileAnalyser;
import ru.edu.lecture3.FileAnalysermpl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class FileAnalysermplTests {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    private final String FILE_NAME = "Storage.txt";
    private FileAnalyser fileAnalyser = new FileAnalysermpl("Storage.txt");

    @Before
    public void before_Tests() {
        try {
            FileWriter writer = new FileWriter(FILE_NAME);
            writer.write("test");
            writer.append('\n');
            writer.write("symbol input");
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


     /**
     *Должен выполниться успешно getFileName()
     */

    @Test
    public void getFileName_Test() {
        Assert.assertEquals("Имена файлов не совпадают", FILE_NAME, fileAnalyser.getFileName());
    }


     /**
     *Должен выполниться неуспешно getFileName()
     */

    @Test(expected = RuntimeException.class)
    public void getFileName_Negative_Test() {
        fileAnalyser = new FileAnalysermpl("Error.txt");
        fileAnalyser.getFileName();
        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Файл отсутствует");
    }

     /**
     *Должен выполниться успешно getRowsCount()
     */

    @Test
    public void getRowsCount_Test() {
        Assert.assertEquals(2, fileAnalyser.getRowsCount());
    }


     /**
     *Должен выполниться неуспешно getRowsCount()
     */

    @Test
    public void getRowsCount_Negative_Test() {
        fileAnalyser = new FileAnalysermpl("Storage.txt");
        Assert.assertNotEquals("Некорректное количество строк", 0, fileAnalyser.getRowsCount());
    }

     /**
     *Должен выполниться успешно getLettersCount()
     */

    @Test
    public void getLettersCount_Test() {
        int docLength = 17;
        Assert.assertEquals(docLength, fileAnalyser.getLettersCount());
    }

     /**
     *Должен выполниться неуспешно getLettersCount()
     */

    @Test
    public void getLettersCount_Negative_Test() {
        fileAnalyser = new FileAnalysermpl("Storage.txt");
        Assert.assertNotEquals("Некорректное количество символов", 1, fileAnalyser.getLettersCount());
    }


     /**
     *Должен выполниться успешно getSymbolsStatistics()
     */

    @Test
    public void getSymbolsStatistics_Test() {
        Map<Character, Integer> map = fileAnalyser.getSymbolsStatistics();
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            Integer count = entry.getKey().equals('t') ? 3 : entry.getKey().equals('s') ? 2 : 1;
            Assert.assertEquals(count, entry.getValue());
        }
    }

     /**
     *Должен выполниться неуспешно getSymbolsStatistics()
     */

    @Test(expected = RuntimeException.class)
    public void getSymbolsStatistics_Negative_Test() {
            fileAnalyser = new FileAnalysermpl("abcd.txt");
            fileAnalyser.getSymbolsStatistics();
            exceptionRule.expect(RuntimeException.class);
            exceptionRule.expectMessage("Файл отсутствует или пуст");
    }


     /**
     *Должен выполниться успешно getTopNPopularSymbols()
     */

    @Test
    public void getTopNPopularSymbols_Test() {
        List<Character> symbols = Arrays.asList('s', 't');
        List<Character> popularSymbols = fileAnalyser.getTopNPopularSymbols(symbols.size());
        Assert.assertEquals(symbols.size(), popularSymbols.size());
        for (Character symbol : symbols) {
            Assert.assertTrue("Список не содержит ожидаемых символов.", popularSymbols.contains(symbol));
        }
    }


     /**
     *Должен выполниться неуспешно getTopNPopularSymbols()
     */

    @Test(expected = RuntimeException.class)
    public void getTopNPopularSymbols_Negative_Test() {
        fileAnalyser = new FileAnalysermpl("abcd.txt");
        List<Character> symbols = Arrays.asList('s', 'n');
        fileAnalyser.getTopNPopularSymbols(symbols.size());
        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Файл отсутствует или пуст");

    }


     /**
     *Должен выполниться успешно saveSummary()
     */

    @Test
    public void saveSummary_Test() {
        fileAnalyser.saveSummary("result_test.txt");
        File file = new File("result_test.txt");
        Assert.assertTrue(file.exists());
        Assert.assertTrue(file.isFile());
    }


     /**
     *Должен выполниться неуспешно saveSummary()
     */

    @Test(expected = RuntimeException.class)
    public void saveSummary_Negative_Test() {
            fileAnalyser = new FileAnalysermpl("abcd.txt");
            fileAnalyser.saveSummary("result_test.txt");
            exceptionRule.expect(RuntimeException.class);
            exceptionRule.expectMessage("Файл отсутствует или пуст");
    }

}